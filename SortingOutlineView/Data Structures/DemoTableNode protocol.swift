//
//  DemoTableNode protocol.swift
//  SortingOutlineView
//
//  Created by Alexander Momchilov on 2020-08-10.
//  Copyright © 2020 Alexander Momchilov. All rights reserved.
//

import Foundation

@objc protocol DemoTableNode {
	@objc var groupName: String { get }
	@objc var id: Int { get }
	@objc var randomNumber: Int { get }
}
