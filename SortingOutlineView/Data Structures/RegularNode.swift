//
//  Data GroupNode.swift
//  SortingOutlineView
//
//  Created by Alexander Momchilov on 2020-08-10.
//  Copyright © 2020 Alexander Momchilov. All rights reserved.
//

import AppKit
class RegularNode: NSObject, DemoTableNode {
	@objc let groupName: String
	@objc let id: Int
	@objc let randomNumber: Int
	
	init(groupName: String, id: Int, randomNumber: Int) {
		self.groupName = groupName
		self.id = id
		self.randomNumber = randomNumber
	}
}

extension RegularNode: OutlineViewNode {
	var childNodes: [OutlineViewNode] {
		get { [] }
		set { }
	}
}
