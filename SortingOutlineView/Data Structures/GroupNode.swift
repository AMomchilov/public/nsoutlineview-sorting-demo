//
//  Data GroupNode.swift
//  SortingOutlineView
//
//  Created by Alexander Momchilov on 2020-08-10.
//  Copyright © 2020 Alexander Momchilov. All rights reserved.
//

import AppKit

class GroupNode: NSObject, DemoTableNode {
	@objc let groupName: String
	
	// This is the main issue. If these 2 fields didn't exist, then the tree couldn't be sorted at
	// this level, because `GroupNode` only has a `groupName` (because that's the only value it
	// cares about, that's all the group row shows), but not an `id` or `randomNumber`
	@objc var id: Int { 123 }
	@objc var randomNumber: Int { 123 }
	
	var regularNodes: [RegularNode]
	
	init(groupName: String, regularNodes: [RegularNode]) {
		self.groupName = groupName
		self.regularNodes = regularNodes
	}
	
}

extension GroupNode: OutlineViewNode {
	var childNodes: [OutlineViewNode] {
		get { self.regularNodes }
		set { self.regularNodes = newValue as! [RegularNode] }
	}
}

extension GroupNode: OutlineViewGroupNode {
	var groupRowCellIdentifier: NSUserInterfaceItemIdentifier {
		return NSUserInterfaceItemIdentifier("plain text")
	}
	
	func renderAsGroupRow(in view: NSTableCellView) {
		view.textField!.stringValue = self.groupName
	}
}
