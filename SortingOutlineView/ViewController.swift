//
//  ViewController.swift
//  SortingOutlineView
//
//  Created by Alexander Momchilov on 2020-08-10.
//  Copyright © 2020 Alexander Momchilov. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {

	@IBOutlet var outlineView: NSOutlineView!
	
	var outlineViewDataSource: OutlineViewDataSource! {
		didSet { self.outlineView.dataSource = outlineViewDataSource }
	}
	
	var outlineViewDelegate: OutlineViewDelegate<DemoTableNode>! {
		didSet { self.outlineView.delegate = outlineViewDelegate }
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.outlineViewDataSource = DemoOutlineViewDatasource(rootObjects: demoData())
		self.outlineViewDelegate = DemoOutlineViewDelegate()
		
		self.outlineViewDelegate.configureColumns(of: self.outlineView)
		self.outlineView.expandItem(nil, expandChildren: true)
	}

	override var representedObject: Any? {
		didSet {
		// Update the view, if already loaded.
		}
	}


}

