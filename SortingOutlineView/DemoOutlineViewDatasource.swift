//
//  DemoOutlineViewDatasource.swift
//  SortingOutlineView
//
//  Created by Alexander Momchilov on 2020-08-10.
//  Copyright © 2020 Alexander Momchilov. All rights reserved.
//

import AppKit


extension Array {
	mutating func sort(using descriptors:  [NSSortDescriptor]) {
		self = self.sorted(using: descriptors)
	}
	
	func sorted(using descriptors: [NSSortDescriptor]) -> Array {
		(self as NSArray).sortedArray(using: descriptors) as! Array
	}
}


class DemoOutlineViewDatasource: OutlineViewDataSource {
	public func outlineView(_ outlineView: NSOutlineView,
							sortDescriptorsDidChange oldDescriptors: [NSSortDescriptor]) {

		let descriptors = outlineView.sortDescriptors
		
		// This is really nice because it doesn't need to treat group rows any differently
		// than the rest
		self.visitDepthFirst { node in
			node.childNodes.sort(using: descriptors)
		}
		
		outlineView.reloadData()
	}
}
