//
//  Sample Data.swift
//  SortingOutlineView
//
//  Created by Alexander Momchilov on 2020-08-10.
//  Copyright © 2020 Alexander Momchilov. All rights reserved.
//

import AppKit

func demoData() -> [GroupNode] {
	func randomNumber() -> Int { (0...100).randomElement()! }
	
	return [
		GroupNode(
			groupName: "Group A",
			regularNodes: [
				RegularNode(
					groupName: "Group A",
					id: 1,
					randomNumber: randomNumber()
				),
				RegularNode(
					groupName: "Group A",
					id: 2,
					randomNumber: randomNumber()
				),
				RegularNode(
					groupName: "Group A",
					id: 3,
					randomNumber: randomNumber()
				),
				RegularNode(
					groupName: "Group A",
					id: 4,
					randomNumber: randomNumber()
				),
				RegularNode(
					groupName: "Group A",
					id: 5,
					randomNumber: randomNumber()
				),
			]
		),
		GroupNode(
			groupName: "Group B",
			regularNodes: [
				RegularNode(
					groupName: "Group B",
					id: 6,
					randomNumber: randomNumber()
				),
				RegularNode(
					groupName: "Group B",
					id: 7,
					randomNumber: randomNumber()
				),
				RegularNode(
					groupName: "Group B",
					id: 8,
					randomNumber: randomNumber()
				),
				RegularNode(
					groupName: "Group B",
					id: 9,
					randomNumber: randomNumber()
				),
				RegularNode(
					groupName: "Group B",
					id: 10,
					randomNumber: randomNumber()
				),
			]
		),
		GroupNode(
			groupName: "Group C",
			regularNodes: [
				RegularNode(
					groupName: "Group C",
					id: 11,
					randomNumber: randomNumber()
				),
				RegularNode(
					groupName: "Group C",
					id: 12,
					randomNumber: randomNumber()
				),
				RegularNode(
					groupName: "Group C",
					id: 13,
					randomNumber: randomNumber()
				),
				RegularNode(
					groupName: "Group C",
					id: 14,
					randomNumber: randomNumber()
				),
				RegularNode(
					groupName: "Group C",
					id: 15,
					randomNumber: randomNumber()
				),
			]
		),
	]
}
