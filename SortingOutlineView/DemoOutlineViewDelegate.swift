//
//  DemoOutlineViewDelegate.swift
//  SortingOutlineView
//
//  Created by Alexander Momchilov on 2020-08-10.
//  Copyright © 2020 Alexander Momchilov. All rights reserved.
//

import AppKit

class DemoOutlineViewDelegate: OutlineViewDelegate<DemoTableNode> {
	init() {
		let columnConfigurations = [
			TableColumnConfiguration<DemoTableNode>(
				name: "Group",
				sortKeyPath: \.groupName,
				cellIdentifier: NSUserInterfaceItemIdentifier("plain text"),
				cellRenderer: { cell, node in cell.textField!.stringValue = node.groupName }
			),
			TableColumnConfiguration<DemoTableNode>(
				name: "ID",
				sortKeyPath: \.id,
				cellIdentifier: NSUserInterfaceItemIdentifier("plain text"),
				cellRenderer: { cell, node in cell.textField!.stringValue = String(node.id) }
			),
			TableColumnConfiguration<DemoTableNode>(
				name: "Random Value",
				sortKeyPath: \.randomNumber,
				cellIdentifier: NSUserInterfaceItemIdentifier("plain text"),
				cellRenderer: { cell, node in cell.textField!.stringValue = String(node.randomNumber) }
			),
		]
		
		super.init(columnConfigurations: columnConfigurations)
	}
}
