//
//  OutlineViewDelegate.swift
//  SystemMonitor
//
//  Created by Alexander Momchilov on 2020-07-17.
//  Copyright © 2020 Alexander Momchilov. All rights reserved.
//

import AppKit

/// A general `NSOutlineViewDelegate`and that uses `TableColumnConfiguration` objects to configure column behaviour
/// and cell behaviour.
///
/// `Model`: A type used to access row values for rendering cells in columns (for non group rows).
///
/// - Attention: Subclasses need to explicitly specify `@objc` for methods looking to satisfy `NSOutlineViewDelegate`
/// conformance. This is because generic classes don't infer `objc` properly on optional requirements of ObjC protocols.
/// Otherwise, such methods will be considered un-implemented, and will never be called (without a compiler error).
/// https://forums.swift.org/t/38533
public class OutlineViewDelegate<Model>: NSObject, NSOutlineViewDelegate {
	let columnConfigurations: [NSUserInterfaceItemIdentifier: TableColumnConfiguration<Model>]
	
	init(columnConfigurations: [TableColumnConfiguration<Model>]) {
		self.columnConfigurations = Dictionary(uniqueKeysWithValues:
			columnConfigurations.map { (key: $0.id, value: $0 ) }
		)
	}

	// MARK: - NSOutlineViewDelegate
	// (ObjC protocol conformances cannot be put in extensions to generic types)
	
	public func outlineView(_ outlineView: NSOutlineView,
							isGroupItem item: Any) -> Bool {
		item is OutlineViewGroupNode
	}
	
	public func outlineView(_ outlineView: NSOutlineView,
				   viewFor tableColumn: NSTableColumn?,
				   item: Any) -> NSView? {
		
		guard let tableColumn = tableColumn else {
			// This row is a group row
			
			let groupRowNode = item as! OutlineViewGroupNode
			
			let view = outlineView.makeView(withIdentifier: groupRowNode.groupRowCellIdentifier,
											owner: nil) as! NSTableCellView
			
			groupRowNode.renderAsGroupRow(in: view)
			
			return view
		}

		if let columnConfig = columnConfigurations[tableColumn.identifier] {
			let view = outlineView.makeView(withIdentifier: columnConfig.cellIdentifier,
											owner: nil) as! NSTableCellView

			let node = item as! Model
			
			columnConfig.cellRenderer(view, node)
			
			return view
		}

		fatalError("Forgot to implement a column configuration for \(tableColumn.identifier.rawValue)")
	}
}

extension OutlineViewDelegate: TableColumnConfigurator {}
