//
//  OutlineViewNode.swift
//  SystemMonitor
//
//  Created by Alexander Momchilov on 2020-08-08.
//  Copyright © 2020 Alexander Momchilov. All rights reserved.
//

import AppKit

// Requires NSObjectProtocol for the purpose of key-value-coding based sorting
public protocol OutlineViewNode: NSObjectProtocol {
	var childNodes: [OutlineViewNode] { get set }
	
	var isExpandable: Bool { get }
}

extension OutlineViewNode {
	var isExpandable: Bool { !self.childNodes.isEmpty }
}

public protocol OutlineViewGroupNode {
	var groupRowCellIdentifier: NSUserInterfaceItemIdentifier { get }

	func renderAsGroupRow(in view: NSTableCellView)
}
