//
//  OutlineViewDelegate.swift
//  SystemMonitor
//
//  Created by Alexander Momchilov on 2020-07-17.
//  Copyright © 2020 Alexander Momchilov. All rights reserved.
//

import AppKit


/// A general `NSOutlineViewDatasource` that uses reads an object graph of `OutlineViewNode`s
class OutlineViewDataSource: NSObject {
	
	var rootNodes: [OutlineViewNode]
	
	init(rootObjects: [OutlineViewNode]) {
		self.rootNodes = rootObjects
	}
}

extension OutlineViewDataSource {
	/// Returns an `OutlineViewNode` for the provided `item`
	/// - Parameter item: the `item` as provided to one of the `NSOutlineViewDataSource` methods' parameters
	/// - Returns: Either `self` (as a `OutlineViewNode`, which serves its root nodes as children),
	///            or `item` casted to a `OutlineViewNode`
	private func asNode(_ item: Any?) -> OutlineViewNode {
		if item == nil { return self } // NSOutlineView encodes the root as nil
		return item as! OutlineViewNode
	}

	func visitDepthFirst(_ visitor: (OutlineViewNode) -> Void) {
		func visitDepthFirst(node: OutlineViewNode) {
			for child in node.childNodes {
				visitor(child)
				visitDepthFirst(node: child)
			}
		}
		
		visitor(self)
		visitDepthFirst(node: self)
	}
}
	
extension OutlineViewDataSource: NSOutlineViewDataSource {
	
	
	public func outlineView(_ outlineView: NSOutlineView,
							numberOfChildrenOfItem item: Any?) -> Int {
		asNode(item).childNodes.count
	}

	public func outlineView(_ outlineView: NSOutlineView,
							child index: Int, ofItem item: Any?) -> Any {
		asNode(item).childNodes[index]
	}
	
	public func outlineView(_ outlineView: NSOutlineView,
							isItemExpandable item: Any) -> Bool {
		asNode(item).isExpandable
	}
}


// Allows `OutlineViewDataSource` to be passed around as a `OutlineViewNode`, which is useful because it
// can store its own root values, and present them with the same interface as any other node.
extension OutlineViewDataSource: OutlineViewNode {
	var childNodes: [OutlineViewNode] {
		get { self.rootNodes }
		set { self.rootNodes = newValue }
	}
}
