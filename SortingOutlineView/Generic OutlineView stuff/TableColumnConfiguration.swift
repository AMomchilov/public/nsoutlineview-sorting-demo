//
//  TableColumnConfiguration.swift
//  SystemMonitor
//
//  Created by Alexander Momchilov on 2020-07-17.
//  Copyright © 2020 Alexander Momchilov. All rights reserved.
//

import AppKit


/// An object with stores the attributes of an NSTableColumn, for use with an NSTableView or NSOutlineView
/// The `Model` generic object must conform to `NSObject`, in order to be compatible with `NSSortDescriptor`-based sorting.
public struct TableColumnConfiguration<Model> {
	let name: String
	
	let id: NSUserInterfaceItemIdentifier
	let sortDescriptor: NSSortDescriptor
	
	let cellIdentifier: NSUserInterfaceItemIdentifier
	let cellRenderer: (NSTableCellView, Model) -> Void
	
	init<SortValue>(
		name: String,
		sortKeyPath: KeyPath<Model, SortValue>,
		
		cellIdentifier: NSUserInterfaceItemIdentifier,
		cellRenderer: @escaping (NSTableCellView, Model) -> Void
	) {
		self.name = name
		
		self.id = NSUserInterfaceItemIdentifier(name)
		self.sortDescriptor = NSSortDescriptor(keyPath: sortKeyPath, ascending: true)
		
		self.cellIdentifier = cellIdentifier
		self.cellRenderer = cellRenderer
	}
	
	func configure(_ column: NSTableColumn) {
		assert(column.identifier == self.id)
		assert(column.title == self.name)
		
		column.sortDescriptorPrototype = self.sortDescriptor
	}
}

protocol TableColumnConfigurator {
	associatedtype Model
	
	var columnConfigurations: [NSUserInterfaceItemIdentifier: TableColumnConfiguration<Model>] { get }
	func configureColumns(of outlineView: NSTableView)
}

extension TableColumnConfigurator {
	func configureColumns(of tableView: NSTableView) {
		for column in tableView.tableColumns {
			guard let columnConfig = self.columnConfigurations[column.identifier] else {
				let columnId = column.identifier.rawValue
				
				print("""
					ℹ️: The storyboard contains a column with id \"\(columnId)\", but the
					column configuration did not contain a config for that key. Available configs:
					\(self.columnConfigurations.values.map { $0.id.rawValue })
				""")
				
				return
			}
			
			columnConfig.configure(column)
		}
	}
}
